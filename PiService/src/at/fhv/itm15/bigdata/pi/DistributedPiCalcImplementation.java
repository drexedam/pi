package at.fhv.itm15.bigdata.pi;

import java.net.InetAddress;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.jws.WebService;
import javax.servlet.http.HttpServletRequest;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;

@WebService(endpointInterface = "at.fhv.itm15.bigdata.pi.DistributedPiCalcInterface")
public class DistributedPiCalcImplementation implements DistributedPiCalcInterface {
	private static final String preSharedKey = "2i0askw4o";

	private static List<URL> liveServices = Collections.synchronizedList(new ArrayList<URL>());
	private static List<URL> knownServices = Collections.synchronizedList(new ArrayList<URL>());
	private ExecutorService threadPool = Executors.newCachedThreadPool();
	private static Timer timer = new Timer(true);

	private static boolean hostsRegistered = false;

	@Resource
	private WebServiceContext serviceContext;

	@PostConstruct
	public void postConstruct() throws Exception {
		if (!hostsRegistered) {
			hostsRegistered = true;

			knownServices.add(new URL("http://127.0.0.1:8080/PiService"));
			// knownServices.add(new URL("http://snf-35435.vm.okeanos-global.grnet.gr:8080/PiService"));

			// for (URL url : knownServices) {
			// try {
			// AddHost(url.toString());
			// } catch (Exception e) {
			// // drop
			// e.printStackTrace();
			// }
			// }

			// test services
			timer.scheduleAtFixedRate(new TimerTask() {
				@Override
				public void run() {
					UpdateOnlineHosts();
				}
			}, 0, 15 * 60 * 60 * 1000); // 15min
		}
	}

	@Override
	public PiResult Pi(final long iterations) {
		long startTime = System.currentTimeMillis();

		List<Future<PiResult>> requests = new ArrayList<Future<PiResult>>();
		double pi = 0;
		int validResponses = 0;

		// Synchronized needed for iterating
		synchronized (liveServices) {
			for (final URL service : liveServices) {
				requests.add(threadPool.submit(new Callable<PiResult>() {
					@Override
					public PiResult call() throws Exception {
						try {
							return GetSingleResult(service, iterations / liveServices.size());
						} catch (Exception e) {
							liveServices.remove(service);
							throw e;
						}
					}
				}));
			}
		}

		for (Future<PiResult> result : requests) {
			try {
				pi += result.get().getValue();
				validResponses++;
			} catch (Exception e) {
				e.printStackTrace();
				// drop
			}
		}

		if (validResponses > 0) {
			pi = pi / validResponses;
		}

		return new PiResult(pi, System.currentTimeMillis() - startTime, iterations);
	}

	private static PiResult GetSingleResult(URL service, long iterations) throws Exception {
		QName qname = new QName("http://pi.bigdata.itm15.fhv.at/", "PiCalcImplementationService");

		service = new URL(service, service.getPath() + "/picalc?wsdl");

		Service remoteService = Service.create(service, qname);
		PiCalcInterface calcService = remoteService.getPort(PiCalcInterface.class);

		PiResult result = calcService.Pi(iterations);
		if (result == null)
			throw new Exception("Unknown response");
		return result;
	}

	@Override
	public void RegisterHost(String psk, String host) throws Exception {
		if (!preSharedKey.equals(psk))
			throw new IllegalArgumentException("psk is wrong");

		AddHost(host);
	}

	@Override
	public void RegisterSelf(String psk) throws Exception {
		if (!preSharedKey.equals(psk))
			throw new IllegalArgumentException("psk is wrong");

		MessageContext messageContext = serviceContext.getMessageContext();
		HttpServletRequest req = (HttpServletRequest) messageContext.get(MessageContext.SERVLET_REQUEST);

		AddHost(req.getRemoteAddr().toString());
	}

	private static void AddHost(String host) throws Exception {
		if (host == null)
			throw new IllegalArgumentException("host is null");

		URL hostUrl;
		InetAddress address;
		int port = 8080;
		String path = "/PiService";
		try {
			hostUrl = new URL(host);
			host = hostUrl.getHost();
			if (hostUrl.getPort() > 0) {
				port = hostUrl.getPort();
			}
			if (hostUrl.getPath() != null) {
				path = hostUrl.getPath();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			address = InetAddress.getByName(host);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Host not found");
		}

		String servicePath = "http://" + address.getHostAddress() + ":" + port + path;

		hostUrl = new URL(servicePath);

		try {
			if (GetSingleResult(hostUrl, 1) == null)
				throw new Exception();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("no valid response from new remote");
		}

		// Not needed because of Collections.synchronizedList
		// synchronized (liveServices) {
		if (!liveServices.contains(hostUrl)) {
			liveServices.add(hostUrl);
		}
		// }
		// synchronized (knownServices) {
		if (!knownServices.contains(hostUrl)) {
			knownServices.add(hostUrl);
		}
		// }
	}

	@Override
	public String[] GetOnlineHosts() {
		String[] servicesStr = new String[liveServices.size()];
		synchronized (liveServices) {
			for (int i = 0; i < liveServices.size(); i++) {
				servicesStr[i] = liveServices.get(i).toString();
			}
		}
		return servicesStr;
	}

	private void UpdateOnlineHosts() {
		// test if services are really up
		synchronized (liveServices) {
			for (URL url : liveServices) {
				try {
					if (GetSingleResult(url, 1) == null)
						throw new Exception();
				} catch (Exception e) {
					// not up
					e.printStackTrace();
					liveServices.remove(url);
				}
			}
		}

		// try to connect to offline known servers
		synchronized (knownServices) {
			for (URL url : knownServices) {
				if (!liveServices.contains(url)) {
					// test if up
					try {
						if (GetSingleResult(url, 1) == null)
							throw new Exception();
						liveServices.add(url);
					} catch (Exception e) {
						e.printStackTrace(); // not up
					}
				}
			}
		}

		// distribute known services
		List<URL> liveServicesCopy = new ArrayList<URL>();
		synchronized (liveServices) {
			liveServicesCopy.addAll(liveServices);
		}
		for (URL url : liveServicesCopy) {
			try {
				QName qname = new QName("http://pi.bigdata.itm15.fhv.at/", "DistributedPiCalcImplementationService");

				URL serviceUrl = new URL(url, url.getPath() + "/distpicalc?wsdl");

				Service remoteService = Service.create(serviceUrl, qname);
				DistributedPiCalcInterface distService = remoteService.getPort(DistributedPiCalcInterface.class);

				distService.RegisterSelf(preSharedKey);
				for (String otherService : distService.GetOnlineHosts()) {
					AddHost(otherService);
				}
			} catch (Exception e) {
				e.printStackTrace();
				// drop
			}
		}
	}
}
