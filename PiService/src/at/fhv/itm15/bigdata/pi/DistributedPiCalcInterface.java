package at.fhv.itm15.bigdata.pi;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

@WebService
@SOAPBinding(style = Style.RPC)
public interface DistributedPiCalcInterface {
	@WebMethod
	public PiResult Pi(long iterations);

	@WebMethod
	public void RegisterHost(String psk, String host) throws Exception;

	@WebMethod
	public void RegisterSelf(String psk) throws Exception;

	@WebMethod
	public String[] GetOnlineHosts();

}
