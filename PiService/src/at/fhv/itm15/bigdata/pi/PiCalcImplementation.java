package at.fhv.itm15.bigdata.pi;

import java.util.Random;

import javax.jws.WebService;

@WebService(endpointInterface = "at.fhv.itm15.bigdata.pi.PiCalcInterface")
public class PiCalcImplementation implements PiCalcInterface {

	@Override
	public PiResult Pi(long iterations) {
		long startTime = System.currentTimeMillis();
		long inCircle = 0;
		Random r = new Random();
		for (long i = 0; i < iterations; i++) {
			double x = r.nextDouble() * 2 - 1;
			double y = r.nextDouble() * 2 - 1;

			if (Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2)) < 1) {
				inCircle++;
			}
		}
		return new PiResult(inCircle / (double) iterations * 4, System.currentTimeMillis() - startTime, iterations);
	}
}
