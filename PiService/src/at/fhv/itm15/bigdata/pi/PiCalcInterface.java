package at.fhv.itm15.bigdata.pi;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

@WebService
@SOAPBinding(style = Style.RPC)
public interface PiCalcInterface {

	@WebMethod
	public PiResult Pi(long iterations);
}
