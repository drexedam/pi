package at.fhv.itm15.bigdata.pi;

public class PiResult {
	private double value;
	private long duration;
	private long points;

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public long getDuration() {
		return duration;
	}

	public void setDuration(long duration) {
		this.duration = duration;
	}

	public long getPoints() {
		return points;
	}

	public void setPoints(long points) {
		this.points = points;
	}

	public PiResult() {
	}

	public PiResult(double value, long duration, long points) {
		this.value = value;
		this.duration = duration;
		this.points = points;
	}
}
